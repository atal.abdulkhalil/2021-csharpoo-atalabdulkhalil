﻿using System;

namespace Test
{
    class Program
    {
        static void Main(string[] args)
        {
            String naam;
            int leeftijd;

            Console.WriteLine("Wat is uw naam? ");
            naam = Console.ReadLine();
            Console.WriteLine("Hoe oud bent u? ");
            leeftijd = int.Parse(Console.ReadLine());
            
            Console.WriteLine($"U bent {naam} en uw leeftijd is {leeftijd}");
        }
    }
}
