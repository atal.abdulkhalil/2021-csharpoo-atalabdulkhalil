﻿using System;
using System.Collections.Generic;
using Datalaag;
using Globals;
using Logica;

namespace DemoConsole
{
    class Program
    {
        static void Main(string[] args)
        {

            LeesCSV obj = new LeesCSV();
            Logica.Logica logicaObj = new Logica.Logica();

            try
            {
                logicaObj.ProductToevoegen("alcatel", ProductCat.Mobile, 3, 199.95);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            foreach (KeyValuePair<string, Product> author in obj.GeefInfo())
            {
                Console.WriteLine("Key: {0}, Value: {1}",
                    author.Key, author.Value);

            }

            
            Console.WriteLine(logicaObj.ZoekProduct("Iphone"));



        }
    }
}
