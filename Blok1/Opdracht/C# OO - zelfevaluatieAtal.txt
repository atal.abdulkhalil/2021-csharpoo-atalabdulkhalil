C# OO - zelfevaluatie
=========================================

Naam:	<Atal Abdul Khalil>
Datum:  <5 oktober 2020>
Volgnummer blok: <1>

Onderwerp: <voorraadbeheer>

Stand van zaken: 

Ik ben gestart met de toepassing. Ik heb al twee klasse gemaakt: datalaag en producten.
In mijn producten klasse hou ik de gegevens van mijn producten bij en  
via mijn datalaag stuur ik de informatie over mijn producten (prijs, aantal enz..) naar mijn .csv file en ik ook een overzicht vragen 
van alle aanwezige producten. Ik wil ik nog informatie op een bepaalde plaats in mijn file kunnen wijzigen of verwijderen en ik ben daarmee bezig, daarna wil ik nog een userinterface aanmaken.



Sterke punten:

Als ik ergens aan vast zit zoek ik het online op en meestal begrijp ook de uitleg maar als ik dat niet begrijp dan vraag ik uitleg aan een  medestudent of een docent.

Aandachtspunten in je aanpak:

Op tijd beginnen en zorgen dat ik mee ben met mijn planning.

Technische aandachtspunten:

Ik weet nog niet hoe dat ik informatie op een bepaalde plaats in mijn file kan wijzigen of verwijderen.

Leerdoelen:

kennismaking met Visual Studio & C# 					toepassing
debugging 								inzicht
programmeerstijl 							toepassing
enum 									toepassing
properties 								toepassing
generic collections 							toepassing
werken met bestanden/streams 						toepassing
architectuur van een toepassing: meerlagenmodel 			toepassing