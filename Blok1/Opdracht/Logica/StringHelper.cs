﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Logica
{
    public static class StringHelper
    {
        public static string EersteLetterNaarHoofdL(this string inputString)
        {
            if(inputString.Length > 0)
            {
                char[] array = inputString.ToCharArray();
                array[0] = char.ToUpper(array[0]);
                return new string(array);
            }
            return inputString;
        }
    }
}
