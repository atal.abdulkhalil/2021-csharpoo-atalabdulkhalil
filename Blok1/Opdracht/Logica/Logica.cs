﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Datalaag;
using Globals;

namespace Logica
{
    public class Logica : ILogica
    {
        LeesCSV dataProvider = new LeesCSV();
        Dictionary<string, Product> products;

        public void ProductToevoegen(string prodName, ProductCat productCategory, int quantity, string price)
        {
            Dictionary<string, Product> readFile = new Dictionary<string, Product>();
            readFile = dataProvider.GeefInfo();
            //double calculatedPrice = BerekenPrijs(productCategory, price);

            if (!readFile.ContainsKey(prodName))
            {
                var newProduct = new Product(prodName, productCategory, quantity, price);

                List<string> output = new List<string>();
                output.Add($"{ newProduct.ProName };{ newProduct.ProductCategory };{ newProduct.Quantity };{ newProduct.Price }");

                File.AppendAllLines(dataProvider.FilePath, output);

            }
            else
            {
                ProductWijzigen(prodName, productCategory, quantity, price, readFile);
            }

        }


        public void ProductWijzigen(string prodName, ProductCat productCategory, int quantity, string price, Dictionary<string, Product> readFile)
        {
            //double calculatedPrice = BerekenPrijs(productCategory, price);
            var updateProd = new Product(prodName, productCategory, quantity, price);
            readFile[prodName] = updateProd;
            List<string> output = new List<string>();

            foreach (KeyValuePair<string, Product> product in readFile)
            {
                var key = product.Key;
                output.Add($"{ readFile[key].ProName };{ readFile[key].ProductCategory };{ readFile[key].Quantity };{ readFile[key].Price }");
            }
            File.WriteAllLines(dataProvider.FilePath, output);
        }


        public List<Product> ZoekProductOpNaam(string query)
        {
            if (string.IsNullOrEmpty(query))
            {
                throw new ArgumentException("Please enter a product name");
            }
            List<Product> list = new List<Product>();
            products = dataProvider.GeefInfo();
            list = products.Values.ToList();

            var result = list.Where(x => x.ProName.Contains(query)).ToList();
            if (result.Count == 0)
            {
                throw new KeyNotFoundException(query + " Not found");
            }
            return result;
        }

        public List<Product> ZoekProductOpCategorie(ProductCat catagorie)
        {
            List<Product> list = new List<Product>();
            products = dataProvider.GeefInfo();
            list = products.Values.ToList();
            var result = (from s in list
                          where s.ProductCategory == catagorie
                          orderby s.ProName
                          select s).ToList();
            if (result.Count == 0)
            {
                throw new KeyNotFoundException(catagorie + " Not found");
            }
            return result;
        }


        //private double BerekenPrijs(ProductCat category, double price)
        //{
        //    double result = 0;
        //    switch (category)
        //    {
        //        case ProductCat.Mobile:
        //            result = price * 0.25 + price;
        //            break;
        //        case ProductCat.Computer:
        //            result = price * 0.15 + price;
        //            break;
        //        case ProductCat.Others:
        //            result = price * 0.1 + price;
        //            break;
        //    }
        //    return result;
        //}
    }
}
