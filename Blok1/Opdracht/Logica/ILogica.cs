﻿using Globals;
using System.Collections.Generic;

namespace Logica
{
    public interface ILogica
    {
        void ProductToevoegen(string prodName, ProductCat productCategory, int quantity, string price);
        void ProductWijzigen(string prodName, ProductCat productCategory, int quantity, string price, Dictionary<string, Product> readFile);
        public List<Product> ZoekProductOpNaam(string query);
        List<Product> ZoekProductOpCategorie(ProductCat catagorie);
    }
}