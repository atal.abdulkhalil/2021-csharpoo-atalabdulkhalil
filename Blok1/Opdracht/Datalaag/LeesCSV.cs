﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Globals;

namespace Datalaag
{
    public class LeesCSV : ILeesCSV
    {
        private Dictionary<string, Product> allProducts = new Dictionary<string, Product>();
        private const string Path = @"Resources/data.csv";
        public string FilePath
        {
            get => Path;
        }


        public Dictionary<string, Product> GeefInfo()
        {
            return allProducts = LeesProductenInfo(Path);
        }

        //bestand lezen
        //Gebaseerd op code gevonden op Les 2B (Bestands-IO, serialisatie)
        private Dictionary<string, Product> LeesProductenInfo(string filename)
        {
            var result = new Dictionary<string, Product>();
            try
            {
                using var sr = new StreamReader(filename);
                string line;
                while (!sr.EndOfStream)
                {
                    line = sr.ReadLine();
                    string[] lineParts = line.Split(';');
                    if (lineParts.Length > 0)
                    {
                        result.Add(lineParts[0], new Product(proName: lineParts[0],
                                                productCategory: Enum.Parse<ProductCat>(lineParts[1]),
                                                quantity: Int16.Parse(lineParts[2]),
                                                price: lineParts[3]
                                               ));
                    }
                }
            }
            catch (IOException ex)
            {
                Console.WriteLine($"The file could not be read:{ex.Message}");
            }
            return result;
        }
    }
}
