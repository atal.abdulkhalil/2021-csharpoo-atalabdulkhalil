﻿using Globals;
using System.Collections.Generic;

namespace Datalaag
{
    public interface ILeesCSV
    {
        string FilePath { get; }

        Dictionary<string, Product> GeefInfo();
    }
}