﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Globals
{
    public class Product : Company
    {
        private String proName;
        private ProductCat productCategory;
        private int quantity;
        private string price;

        public string ProName
        { 
            get => proName;
            private set
            {
                if (!string.IsNullOrEmpty(value))
                    proName = value;
                else
                    throw new ArgumentException("Enter product name!");
            }
        }
        public ProductCat ProductCategory { get => productCategory; set => productCategory = value; }
        public int Quantity
        {
            get
            {
                return quantity;
            }
            private set
            {
                if (value <= 0) throw new ArgumentException("Qunatity must be greater than zero!");
                quantity = value;
            }
        }
        public string Price
        {
            get
            {
                return price;
            }
            private set
            {
                if (string.IsNullOrEmpty(value)) throw new ArgumentException("Give a valid price!");
                price = value;
            }
        }

        public Product() : base()
        {
            this.proName = "";
            this.quantity = 0;
            this.price = "";
        }

        public Product(string proName, ProductCat productCategory, int quantity, string price) : base()
        {
            this.ProName = proName;
            this.ProductCategory = productCategory;
            this.Quantity = quantity;
            this.Price = price + base.Euro;
        }


        public override string ToString()
        {
            return $"Product Naam: {proName} Categorie: {productCategory} Aantal: {quantity} Prijs: {price}";
        }
    }
}
