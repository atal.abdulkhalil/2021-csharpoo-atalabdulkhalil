﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Globals;
using Datalaag;
using Logica;

namespace InventoryGUI
{
    public partial class Inventory : Form
    {
        ILogica logicaObj;
        ILeesCSV obj;
        //LeesCSV obj = new LeesCSV();
        //Logica.Logica logicaObj = new Logica.Logica();
        public Inventory(ILogica logica, ILeesCSV leesCSV)
        {
            InitializeComponent();
            this.logicaObj = logica;
            this.obj = leesCSV;
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            

            foreach (KeyValuePair<string, Product> itme in obj.GeefInfo())
            {
                var key = itme.Key;
                string [] prodoct  = { obj.GeefInfo()[key].ProName, obj.GeefInfo()[key].ProductCategory.ToString(), obj.GeefInfo()[key].Quantity.ToString(), obj.GeefInfo()[key].Price.ToString() };
                var listview = new ListViewItem(prodoct);
                listView.Items.Add(listview);

            }
        }

        private void listView_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void buttonAdd_Click(object sender, EventArgs e)
        {
            try
            {
                if (comboBoxCat.Text.Length < 2)
                {
                    throw new ArgumentNullException("Please choose a category!");
                }
                string prdName = textBoxName.Text;
                ProductCat category = Enum.Parse<ProductCat>(comboBoxCat.Text);
                int quant = Int16.Parse(textBoxQuant.Text);
                string price = textBoxPrice.Text;
                logicaObj.ProductToevoegen(prdName, category, quant, price);

                listView.Items.Clear();

                foreach (KeyValuePair<string, Product> itme in obj.GeefInfo())
                {
                    var key = itme.Key;
                    string[] prodoct = { obj.GeefInfo()[key].ProName, obj.GeefInfo()[key].ProductCategory.ToString(), obj.GeefInfo()[key].Quantity.ToString(), obj.GeefInfo()[key].Price.ToString() };
                    var listview = new ListViewItem(prodoct);
                    listView.Items.Add(listview);

                }
            }
            catch (FormatException ex)
            {
                errorMsg.Text = ex.Message;
            }
            catch (ArgumentException ex)
            {
                errorMsg.Text = ex.Message;
            }
            catch (Exception ex)
            {
                errorMsg.Text = ex.Message;
            }
        }

        private void buttonSearch_Click(object sender, EventArgs e)
        {
            try
            {
                listView.Items.Clear();
                string srch = textBoxSearch.Text;
                List<Product> results = logicaObj.ZoekProductOpNaam(srch);
                foreach (var result in results)
                {
                    string[] prodoct = { result.ProName, result.ProductCategory.ToString(), result.Quantity.ToString(), result.Price.ToString() };
                    var listview = new ListViewItem(prodoct);
                    listView.Items.Add(listview);
                }
            }
            catch (ArgumentException ex)
            {
                errorMsg.Text = ex.Message;
            }
            catch (Exception ex)
            {
                errorMsg.Text = ex.Message;
            }
        }

        private void buttonSrchCat_Click(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(textBoxSearch.Text))
                {
                    throw new ArgumentNullException("Please enter a product name!");
                }
                listView.Items.Clear();
                ProductCat srch = Enum.Parse<ProductCat>(textBoxSearch.Text.EersteLetterNaarHoofdL());
                List<Product> results = logicaObj.ZoekProductOpCategorie(srch);
                foreach (var result in results)
                {
                    string[] prodoct = { result.ProName, result.ProductCategory.ToString(), result.Quantity.ToString(), result.Price.ToString() };
                    var listview = new ListViewItem(prodoct);
                    listView.Items.Add(listview);
                }
            }
            catch (ArgumentException ex)
            {
                errorMsg.Text = ex.Message;
            }
            catch (Exception ex)
            {
                errorMsg.Text = ex.Message;
            }
        }
    }
}
