# 2021-CSharpOO-AtalAbdulkhalil

## Deze tachnieken heb ik in blok1 verwerkt of aangepast.
##### -Interfaces
##### -Architectuur van een toepassing - Meerlagenmodel
##### -Klassen - klassehiërarchie
##### -Extension methods
##### -Language Integrated Query (Linq)
##### -(Statische klassen, methoden, velden) (blok1 en blok2)

## Deze tachnieken heb ik in blok2 verwerkt.
##### - Delegates (in mijn GUI laag als je op de konp tonen klikt bij (overleden in het totaal))
##### - Lambda expressions (in mijn GUI laag als je op de konp tonen klikt bij (overleden in het totaal))
##### - Events
##### - Concurrent programmatie: Tasks
##### - Concurrent programming: Task synchronisation
##### - Asynchronous programming: async .. await
##### - (Recursie)
##### -(Statische klassen, methoden, velden) (blok1 en blok2)


## Feedback C# OO Programming


#### Kennismaking Visual Studio 2019

- [x] *Aanmaken solution binnen GIT-repository*
- [x] *Eenvoudige consoletoepassing (met invoer gebruiker)*
- [x] *Eenvoudige WinForm toepassing*
- [x] *Een klasse toevoegen*

#### Gebruik GIT

- [ ] *Je gebruikt 'atomaire' commits*
- [x] *Je voert voldoende frequent een 'Commit All & Sync' uit.*
- [x] *Je gebruikt zinvolle commit messages*

* Maak er een goede gewoonte van om meerdere commits te doen tijdens een 'werksessie', op basis van afgewerkte 'atomaire' deeltaken, telkens met gepaste commit message

#### Debugging

--> Nog niet beoordeeld (kan enkel interactief gebeuren)

- [ ] *Code stap voor stap uitvoeren*
- [ ] *Breakpoints*
- [ ] *De waarde van variabelen bekijken tijdens de uitvoering van je programma*

#### Programmeerstijl

- [ ] *Huisregels voor programmeerstijl volgen*

* Hanteer de gevraagde volgorde voor de verschillende onderdelen van je klasse (properties na de velden en voor de constructor).
* Voor constanten wordt PascalCasing gebruikt. (cf. https://github.com/ktaranov/naming-convention/blob/master/C%23%20Coding%20Standards%20and%20Naming%20Conventions.md)
* Verwijder methoden met dummy inhoud (bv. via designer aangemaakte event handling methoden die je niet verder nodig hebt)
* Gebruik geen underscores in namen (pas de automatisch gegenereerde namen met underscores aan).

 
#### Exceptions

- [x] *try..catch*
- [ ] *try..catch..finally*
- [x] *Je werpt bruikbare exceptions op wanneer je een foutsituatie detecteert die niet lokaal op een beter manier kan afgehandeld worden.*

#### Enumerations

- [x] *Declaratie en gebruik van enum-type*



 
#### Properties

- [x] *Full property (with private backing field)*
- [x] *Auto-implemented property*
- [ ] *Access-modifiers voor Getters en Setters*

#### Interpolated strings

- [x] *Interpolated strings*

#### Generic collections

- [x] *List<T>*
- [x] *Dictionary<T,T>*
- [ ] *Overzicht andere generic collections*

#### Interfaces

- [x] *Interface declaratie*
- [x] *Interface implementatie*
- [ ] *Interface gebruiken als type*

#### Architectuur van een toepassing - Meerlagenmodel

- [x] *Klasseblibliotheken*
- [x] *Meerlagenmodel - 3lagenmodel*
- [x] *'Loose coupling' - dependency injection*
- [x] *Interface gebruiken als scheiding tussen architectuurlagen*

* Verzorg de communicatie tussen de 3 lagen van het drielagenmodel als volgt: PL > BL > DL. Laat de GUI m.a.w. niet rechtstreeks met de datalaag praten.


#### Bestanden en 'streams'

- [x] *Statische klassen uit 'System.IO'*
- [x] *Streams*
- [ ] *Serialisatie*


#### 'Value' en 'Reference' types, cloning van objecten

--> Nog niet beoordeeld

- [ ] *'value' en 'reference' types, 'deep' versus 'shallow' copy*
- [ ] *object cloning*

#### Klassen - klassehi�rarchie

- [x] *Klasse declaratie - constructoren*
- [x] *constructor overloading*
- [ ] *Klasse-hi�rarchie*

kvas-6nov:
* constructor overloading, maar slechts 1 constructor gebruikt*
* Een product IS GEEN company => geen correcte overervingsrelatie : class Product : Company


#### Structs

--> Nog niet beoordeeld

- [ ] *Structs*

##### Extension methods

- [x] *Extension method schrijven*
- [x] *Functioneel gebruik van extension methods*

#### Delegates

- [x] *Delegates*

#### Lambda expressions

- [x] *Lambda expressions*

#### Language Integrated Query (Linq)

- [x] *Linq standard query operator syntax*
- [x] *Linq method syntax*
- [ ] *Basismethodes voor Linq*

kvas-6nov:
.Sum()

#### Events

- [x] *Event 'Publisher'*
- [x] *Event 'Consumer'*

#### Concurrent programmatie: Tasks

- [x] *Tasks*
- [x] *Cross-thread' interactie vanuit een Task met de userinterface*
- [x] *Exceptions in Tasks*
- [x] *Parallel loops*
 
* public async Task<List<CvirusCountry>> LoadCoronaResults()
* var task1 = Task.Factory.StartNew(() => dataProvider.BerekenSterfPercentage(item, token));


#### Concurrent programming: Task synchronisation

- [x] *Lock*
- [ ] *Concurrent/ thread safe collections*

#### Asynchronous programming: async .. await

- [x] *async .. await*

#### (Recursie)

- [x] *Recursie - concept*
- [ ] *Backtracking*

* public int Veelvoudigen(int getal, int macht)

#### (Indexers & Iteratoren)
 
--> Nog niet beoordeeld

- [ ] *Indexers*
- [ ] *Enumeratoren*

#### (Statische klassen, methoden, velden)

- [ ] *Statische klassen, methoden, velden*

kvas-6nov: ?andere dan static Main en extension method

#### Code Reviews

- [x] *Code reviews*

* Plaats je zelfevaluatie en verkregen review in map 'Solution Items' van je VS solution, cf. richtlijnen document "Installatie & gebruik Visual studio 2019 en GIT.pdf", sectie 'Documentatie over je oplossing'
