﻿using System;

namespace Globals
{
    public class CvirusCountry
    {
        

        public string infected { get; }
        public string recovered { get; }
        public string deceased { get; }
        public string country { get; }

        public CvirusCountry(string infected, string recovered, string deceased, string country)
        {
            this.infected = infected;
            this.recovered = recovered;
            this.deceased = deceased;
            this.country = country;
        }

        public override string ToString()
        {
            return $"{infected} {recovered} {deceased} {country}";
        }
    }
}
