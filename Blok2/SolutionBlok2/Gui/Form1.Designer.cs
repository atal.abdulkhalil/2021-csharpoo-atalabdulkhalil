﻿namespace Gui
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.listView1 = new System.Windows.Forms.ListView();
            this.infected = new System.Windows.Forms.ColumnHeader();
            this.recovered = new System.Windows.Forms.ColumnHeader();
            this.deceased = new System.Windows.Forms.ColumnHeader();
            this.country = new System.Windows.Forms.ColumnHeader();
            this.chanceOfDeath = new System.Windows.Forms.ColumnHeader();
            this.recoveringsChance = new System.Windows.Forms.ColumnHeader();
            this.buttonLaad = new System.Windows.Forms.Button();
            this.evenMsg = new System.Windows.Forms.Label();
            this.buttonLand = new System.Windows.Forms.Button();
            this.TotalKans = new System.Windows.Forms.Button();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.buttonPowerof = new System.Windows.Forms.Button();
            this.textBoxPowerof = new System.Windows.Forms.TextBox();
            this.labelError = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // listView1
            // 
            this.listView1.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.infected,
            this.recovered,
            this.deceased,
            this.country,
            this.chanceOfDeath,
            this.recoveringsChance});
            this.listView1.FullRowSelect = true;
            this.listView1.GridLines = true;
            this.listView1.HideSelection = false;
            this.listView1.Location = new System.Drawing.Point(1334, 249);
            this.listView1.Name = "listView1";
            this.listView1.Size = new System.Drawing.Size(1250, 807);
            this.listView1.TabIndex = 0;
            this.listView1.UseCompatibleStateImageBehavior = false;
            this.listView1.View = System.Windows.Forms.View.Details;
            // 
            // infected
            // 
            this.infected.Text = "Besmet";
            this.infected.Width = 350;
            // 
            // recovered
            // 
            this.recovered.Text = "Hersteld";
            this.recovered.Width = 150;
            // 
            // deceased
            // 
            this.deceased.Text = "Overleden";
            this.deceased.Width = 160;
            // 
            // country
            // 
            this.country.Text = "Land";
            this.country.Width = 150;
            // 
            // chanceOfDeath
            // 
            this.chanceOfDeath.Text = "Sterfkans";
            this.chanceOfDeath.Width = 150;
            // 
            // recoveringsChance
            // 
            this.recoveringsChance.Text = "Overlevingskans";
            this.recoveringsChance.Width = 250;
            // 
            // buttonLaad
            // 
            this.buttonLaad.Location = new System.Drawing.Point(1665, 63);
            this.buttonLaad.Name = "buttonLaad";
            this.buttonLaad.Size = new System.Drawing.Size(428, 58);
            this.buttonLaad.TabIndex = 1;
            this.buttonLaad.Text = "Laad coronavirus API";
            this.buttonLaad.UseVisualStyleBackColor = true;
            this.buttonLaad.Click += new System.EventHandler(this.buttonLaad_Click);
            // 
            // evenMsg
            // 
            this.evenMsg.AutoSize = true;
            this.evenMsg.ForeColor = System.Drawing.Color.Green;
            this.evenMsg.Location = new System.Drawing.Point(1383, 169);
            this.evenMsg.Name = "evenMsg";
            this.evenMsg.Size = new System.Drawing.Size(943, 41);
            this.evenMsg.TabIndex = 2;
            this.evenMsg.Text = "Druk voordat u de applicatie gebruikt eerst op de bovenstaande konp";
            this.evenMsg.Click += new System.EventHandler(this.evenMsg_Click);
            // 
            // buttonLand
            // 
            this.buttonLand.Location = new System.Drawing.Point(587, 332);
            this.buttonLand.Name = "buttonLand";
            this.buttonLand.Size = new System.Drawing.Size(188, 58);
            this.buttonLand.TabIndex = 3;
            this.buttonLand.Text = "Tonen";
            this.buttonLand.UseVisualStyleBackColor = true;
            this.buttonLand.Click += new System.EventHandler(this.buttonLand_Click);
            // 
            // TotalKans
            // 
            this.TotalKans.Location = new System.Drawing.Point(848, 544);
            this.TotalKans.Name = "TotalKans";
            this.TotalKans.Size = new System.Drawing.Size(188, 58);
            this.TotalKans.TabIndex = 4;
            this.TotalKans.Text = "Tonen";
            this.TotalKans.UseVisualStyleBackColor = true;
            this.TotalKans.Click += new System.EventHandler(this.button1_Click);
            // 
            // buttonCancel
            // 
            this.buttonCancel.Location = new System.Drawing.Point(848, 332);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(188, 58);
            this.buttonCancel.TabIndex = 5;
            this.buttonCancel.Text = "Annuleren";
            this.buttonCancel.UseVisualStyleBackColor = true;
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(121, 249);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(926, 41);
            this.label1.TabIndex = 6;
            this.label1.Text = "Sterf percentage en overlevings percentage in verschellinde landen : ";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(121, 471);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(1061, 41);
            this.label2.TabIndex = 7;
            this.label2.Text = "Hoeveel procent van mensen die besmet zijn geraakt, is overleden in het total :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(121, 698);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(1004, 41);
            this.label3.TabIndex = 8;
            this.label3.Text = "Hoeveel voudig gaat de aantal besmetting worden binnen 3 jaren denk je?";
            // 
            // buttonPowerof
            // 
            this.buttonPowerof.Location = new System.Drawing.Point(794, 782);
            this.buttonPowerof.Name = "buttonPowerof";
            this.buttonPowerof.Size = new System.Drawing.Size(242, 58);
            this.buttonPowerof.TabIndex = 9;
            this.buttonPowerof.Text = "Toon resultaat";
            this.buttonPowerof.UseVisualStyleBackColor = true;
            this.buttonPowerof.Click += new System.EventHandler(this.buttonPowerof_Click);
            // 
            // textBoxPowerof
            // 
            this.textBoxPowerof.Location = new System.Drawing.Point(145, 788);
            this.textBoxPowerof.Name = "textBoxPowerof";
            this.textBoxPowerof.Size = new System.Drawing.Size(250, 47);
            this.textBoxPowerof.TabIndex = 10;
            // 
            // labelError
            // 
            this.labelError.AutoSize = true;
            this.labelError.ForeColor = System.Drawing.Color.Red;
            this.labelError.Location = new System.Drawing.Point(121, 933);
            this.labelError.Name = "labelError";
            this.labelError.Size = new System.Drawing.Size(97, 41);
            this.labelError.TabIndex = 11;
            this.labelError.Text = "label4";
            this.labelError.Visible = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(17F, 41F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(2666, 1086);
            this.Controls.Add(this.labelError);
            this.Controls.Add(this.textBoxPowerof);
            this.Controls.Add(this.buttonPowerof);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.buttonCancel);
            this.Controls.Add(this.TotalKans);
            this.Controls.Add(this.buttonLand);
            this.Controls.Add(this.evenMsg);
            this.Controls.Add(this.buttonLaad);
            this.Controls.Add(this.listView1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListView listView1;
        private System.Windows.Forms.Button buttonLaad;
        private System.Windows.Forms.ColumnHeader infected;
        private System.Windows.Forms.ColumnHeader recovered;
        private System.Windows.Forms.ColumnHeader deceased;
        private System.Windows.Forms.ColumnHeader country;
        private System.Windows.Forms.Label evenMsg;
        private System.Windows.Forms.Button buttonLand;
        private System.Windows.Forms.ColumnHeader chanceOfDeath;
        private System.Windows.Forms.ColumnHeader recoveringsChance;
        private System.Windows.Forms.Button TotalKans;
        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button buttonPowerof;
        private System.Windows.Forms.TextBox textBoxPowerof;
        private System.Windows.Forms.Label labelError;
    }
}

