﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using Datalaag;
using Globals;

namespace Gui
{
    public partial class Form1 : Form
    {
        private bool apiGeladen = false;
        private DataProvider dataProvider = new DataProvider();
        private List<CvirusCountry> data;
        private double overleden;
        private double besmet;
        private CancellationTokenSource tokenSource = null;

        public Form1()
        {
            InitializeComponent();
            ApiHelper.InitializeClient();
        }

        private async void buttonLaad_Click(object sender, EventArgs e)
        {
            listView1.Items.Clear();
            evenMsg.Text = "Aan het laden";
            dataProvider.ApiLoaded -= ApiLoadingCompleted;
            dataProvider.ApiLoaded += ApiLoadingCompleted;
            data = await dataProvider.LoadCoronaResults();

            foreach (var item in data)
            {
                string[] coronaInfo = { item.infected, item.recovered, item.deceased, item.country };
                var listview = new ListViewItem(coronaInfo);
                listView1.Items.Add(listview);
            }
            apiGeladen = true;
            MessageBox.Show(dataProvider.GeefAantalLanden(data) + " landen zijn aanwezijg in deze lijst");
        }
        private void ApiLoadingCompleted(object source, EventArgs e)
        {
            evenMsg.Text = "Api is geladen";
        }

        private void evenMsg_Click(object sender, EventArgs e)
        {

        }

        private async void buttonLand_Click(object sender, EventArgs e)
        {
            tokenSource = new CancellationTokenSource();
            var token = tokenSource.Token;
            if (apiGeladen)
            {
                listView1.Items.Clear();
                try
                {
                    foreach (var item in data)
                    {
                        var task1 = Task.Factory.StartNew(() => dataProvider.BerekenSterfPercentage(item, token));
                        await task1;
                        string sterfPercent = "" + task1.Result;
                        var task2 = Task.Factory.StartNew(() => dataProvider.BerekenOverlevingsPercentage(item, token));
                        await task2;
                        string OverlevingsPercent = "" + task2.Result;
                        Task.WaitAll(task1, task2);

                        string[] coronaInfo = { item.infected, item.recovered, item.deceased, item.country, sterfPercent, OverlevingsPercent };
                        var listview = new ListViewItem(coronaInfo);
                        listView1.Items.Add(listview);
                    }
                }
                catch (OperationCanceledException ae)
                {
                    evenMsg.Text = "Geannuleerd";
                }
                finally
                {
                    tokenSource.Dispose();
                }
            }
            else MessageBox.Show("Druk eerst op laad coronavirus Api knop");
            
        }

        public double BerekenTotaleSterfpercentage(Func<double, double, double> calculate)
        {
            double result = calculate(overleden, besmet);
            return result;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (apiGeladen)
            {
                overleden = dataProvider.GeefTotaleSterfGevallen(data);
                besmet = dataProvider.GeefTotaleBesmetting(data);

                double result = BerekenTotaleSterfpercentage((overleden, besmet) =>
                {
                    var res = (overleden / besmet) * 100;
                    return Math.Ceiling(res);
                });
                MessageBox.Show(result + " %");
            }
            else MessageBox.Show("Druk eerst op laad coronavirus Api knop");
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            if (tokenSource != null)
            {
                tokenSource.Cancel();
            }
            
        }

        private void buttonPowerof_Click(object sender, EventArgs e)
        {
            if (apiGeladen)
            {
                try
                {
                    listView1.Items.Clear();
                    int getal = Int16.Parse(textBoxPowerof.Text);
                    if (getal < 0)
                    {
                        throw new ArgumentException("Geef een geheel getal in aub");
                    }
                    foreach (var item in data)
                    {
                        int result = dataProvider.Veelvoudigen(Convert.ToInt32(item.infected), 2);
                        string[] coronaInfo = { result + "", "", "", item.country };
                        var listview = new ListViewItem(coronaInfo);
                        listView1.Items.Add(listview);
                    }
                }
                catch (Exception ex)
                {
                    labelError.Visible = true;
                    labelError.Text = ex.Message;
                }
            }
            else MessageBox.Show("Druk eerst op laad coronavirus Api knop");
        }
    }
}
