﻿using Newtonsoft.Json;
using System;
using System.Net.Http;
using System.Net.Http.Headers;
using Globals;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Threading;
using System.Linq;

namespace Datalaag
{
    public class DataProvider
    {
        public delegate void ApiLoadedEventHandler(object source, EventArgs args);
        public event ApiLoadedEventHandler ApiLoaded;

        public async Task<List<CvirusCountry>> LoadCoronaResults()
        {
            string baseUri = "https://api.apify.com/v2/key-value-stores/tVaYRsPHLjNdNBu7S/records/LATEST?disableRedirect=true";
            await Task.Delay(200);
            using (HttpResponseMessage response = await ApiHelper.ApiClient.GetAsync($"{baseUri}"))
            {
                if (response.IsSuccessStatusCode)
                {
                    string json = await response.Content.ReadAsStringAsync();
                    var data = JsonConvert.DeserializeObject<List<CvirusCountry>>(json);
                    onApiLoaded();
                    return data;
                }
                else
                {
                    throw new Exception(response.ReasonPhrase);
                }
            }
        }

        protected virtual void onApiLoaded()
        {
            ApiLoaded?.Invoke(this, EventArgs.Empty);
        }

        public double GeefAantalLanden(List<CvirusCountry> list)
        {
            double totaleLanden = 0;
            object mutex = new object();
            Parallel.ForEach(list, item =>
            {
                lock (mutex)
                {
                    totaleLanden++;
                }
            });
            return totaleLanden;
        }

        public double BerekenSterfPercentage(CvirusCountry cvirusCountry, CancellationToken token)
        {
            //Thread.Sleep(1000);
            double overleden = 0;
            double besmet = 0;
            if (!(cvirusCountry.deceased == "NA"))
            {
                overleden = Convert.ToDouble(cvirusCountry.deceased);
                besmet = Convert.ToDouble(cvirusCountry.infected);
            }
            if (token.IsCancellationRequested)
            {
                token.ThrowIfCancellationRequested();
            }
            double result = overleden / besmet * 100;
            return Math.Ceiling(result);

        }

        public double BerekenOverlevingsPercentage(CvirusCountry cvirusCountry, CancellationToken token)
        {
            Thread.Sleep(300);
            double hersteld = 0;
            double besmet = 0;
            if (!(cvirusCountry.recovered == "NA"))
            {
                hersteld = Convert.ToDouble(cvirusCountry.recovered);
                besmet = Convert.ToDouble(cvirusCountry.infected);
            }
            if (token.IsCancellationRequested)
            {
                token.ThrowIfCancellationRequested();
            }
            double result = hersteld / besmet * 100;
            return Math.Ceiling(result);
        }

        public double GeefTotaleSterfGevallen(List<CvirusCountry> cvirusCountry)
        {
            List<double> list = new List<double>();
            foreach (var item in cvirusCountry)
            {
                if (!(item.deceased == "NA"))
                {
                    double overleden = Convert.ToDouble(item.deceased);
                    list.Add(overleden);
                }
            }
            double result = list.Sum();
            return result;
        }

        public double GeefTotaleBesmetting(List<CvirusCountry> cvirusCountry)
        {
            List<double> list = new List<double>();
            foreach (var item in cvirusCountry)
            {
                if (!(item.infected == "NA"))
                {
                    double besmet = Convert.ToDouble(item.infected);
                    list.Add(besmet);
                }
            }
            double result = list.Sum();
            return result;
        }

        public int Veelvoudigen(int getal, int macht)
        {
            if (macht == 1)
            {
                return getal;
            }
            return getal * Veelvoudigen(getal, macht - 1);
        }
    }       
}
